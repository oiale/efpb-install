#!/bin/sh

if (( $EUID != 0 )); then
	echo 'Please run this script with elevated privileges (sudo/root).'
	exit 1;
fi

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

INTERACTIVE=false
if [ -z "$1" ]; then
	INTERACTIVE=false
elif [ "$1" = '-i' ]; then
	INTERACTIVE=true
else
	INTERACTIVE=false
fi

echo 'This script will set up efp browser on this alpine system.'

OPERATIONSPROMPT="This script resides in $SCRIPTPATH, and all operations will be performed here."
if [ $INTERACTIVE = true ]; then
	read -ep "$OPERATIONSPROMPT To continue, press ENTER"
else
	echo "$OPERATIONSPROMPT"
fi

apk update
apk add --no-cache curl openrc

printf "\n\nStep #1: Installing python libraries that are needed for efpb."
if [ $INTERACTIVE = true ]; then
	sleep 5s
fi
# python prerequisites
apk add --no-cache python2 wget gcc g++ python2-dev musl-dev freetype-dev jpeg-dev libxml2-dev libxslt-dev
curl https://bootstrap.pypa.io/get-pip.py -o $SCRIPTPATH/get-pip.py
python2 $SCRIPTPATH/get-pip.py
pip install numpy 
pip install matplotlib 
pip install simplejson phpserialize lxml
cd $SCRIPTPATH
wget -O pyxml.tar.gz https://sourceforge.net/projects/pyxml/files/pyxml/0.8.4/PyXML-0.8.4.tar.gz/download
tar -xvf pyxml.tar.gz
cd PyXML-0.8.4 && python setup.py install
cd $SCRIPTPATH
wget -O efpb.tar.gz https://sourceforge.net/projects/efpbrowser/files/efpbrowser/1.6.0/eFPbrowser-1.6.0.tar.gz/download
tar -xvf efpb.tar.gz
cd eFPbrowser-1.6.0/Imaging-1.1.6efp
python setup.py install


printf "\n\nStep #2: Installing and setting up MySQL."
if [ $INTERACTIVE = true ]; then
	sleep 5s
fi
# MySQL setup
apk add --no-cache mysql mysql-client mysql-dev
sed '/st_mysql_options options;/a unsigned int reconnect;' /usr/include/mysql/mysql.h -i.bkp # fix for python client not being updated
pip install mysql-python
/etc/init.d/mariadb setup
rc-service mariadb start
rc-update add mariadb


printf "\n\nStep #3: Installing and setting up apache2"
if [ $INTERACTIVE = true ]; then
	sleep 5s
fi
# Apache server setup (config in /etc/apache2, default location of served files is in /var/www/localhost/htdocs (DocumentRoot))
apk add --no-cache apache2
mkdir /var/www/html && cd $SCRIPTPATH/eFPbrowser-1.6.0/ && cp -r efp /var/www/html
cd /var/www/html && chmod -R 775 efp && chmod 777 efp/cgi-bin/output
sed -i 's/DocumentRoot "\/var\/www\/localhost\/htdocs"/DocumentRoot "\/var\/www\/html"/' /etc/apache2/httpd.conf
sed -i 's/<Directory "\/var\/www\/localhost\/htdocs">/<Directory "\/var\/www\/html">/' /etc/apache2/httpd.conf
sed -i 's/ScriptAlias \/cgi-bin\/ "\/var\/www\/localhost\/cgi-bin\/"/ScriptAlias \/efp\/cgi-bin\/ "\/var\/www\/html\/efp\/cgi-bin\/"/' /etc/apache2/httpd.conf
sed -i 's/<Directory "\/var\/www\/localhost\/cgi-bin">/<Directory "\/var\/www\/html\/efp\/cgi-bin">/' /etc/apache2/httpd.conf
# sed -i 's/#AddHandler cgi-script .cgi/AddHandler cgi-script .cgi/' /etc/apache2/httpd.conf
sed -i 's/#LoadModule cgid_module modules\/mod_cgid.so/LoadModule cgid_module modules\/mod_cgid.so/' /etc/apache2/httpd.conf
sed -i 's/#LoadModule cgi_module modules\/mod_cgi.so/LoadModule cgi_module modules\/mod_cgi.so/' /etc/apache2/httpd.conf
rc-service apache2 start
rc-update add apache2


printf "\n\nStep #4: cleanup"
if [ $INTERACTIVE = true ]; then
	sleep 5s
fi
# Cleanup
apk del gcc g++ python2-dev musl-dev freetype-dev jpeg-dev libxml2-dev libxslt-dev mysql-dev curl wget
apk cache clean
cd $SCRIPTPATH
rm -rf PyXML-0.8.4 eFPbrowser-1.6.0 pyxml.tar.gz efpb.tar.gz
pip cache purge
rm -rf ~/.cache/pip

echo 'Done. You can find sample data at https://sourceforge.net/projects/efpbrowser/files/sample%20data/1.0/samples-1.0.tar.gz/download'