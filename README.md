# efpb-install

Install scripts for efpb: https://sourceforge.net/projects/efpbrowser/

The scripts are organized into different linux distros. Every script has an interactive mode, invoked with '-i' parameter.